//Incluimos las librerias 
#include <Arduino.h>
#include <WiFi.h>
#include <FirebaseESP32.h>

 //Defimos unas 
#define FIREBASE_HOST "esp32-lm35-99837-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "AIzaSyBt1jm7KetdbdQ359oTyMMyxGVVHOPOynw"

FirebaseData FIREBASE;
//Definimos una ruta de Firebase
String ruta = "Sensor" ;
String rutaEstado = "OnOff/Estado/" ;

//Red a cual conectarse 
#define WIFI_SSID "TeoEsp32"
#define WIFI_PASSWORD "123456789"
#define LED 2 
#define PIN_LM35 35
#define ADC_VREF_mV    5000.0 // in millivolt
#define ADC_RESOLUTION 4096.0
#define RELE 32
int valor;

void setup() {

Serial.begin(115200);//velocidad de baudios


pinMode(LED,OUTPUT);
pinMode(RELE, OUTPUT);
WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
Serial.print("Se esta conectando a:");
Serial.println(WIFI_SSID);

while (WiFi.status() != WL_CONNECTED)
{
  delay(500);
  Serial.print(".");
}

Serial.println("");
Serial.println("Wifi conectado");
Serial.println("IP: ");
Serial.println(WiFi.localIP());

Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
Firebase.reconnectWiFi(true);

}
void loop(){

  
// read the ADC value from the temperature sensor
int adcVal = analogRead(PIN_LM35);
// convert the ADC value to voltage in millivolt
float milliVolt = adcVal * (ADC_VREF_mV / ADC_RESOLUTION);
// convert the voltage to the temperature in °C
int tempC = milliVolt / 10;
Firebase.setInt(FIREBASE, ruta, tempC);
Firebase.getBool(FIREBASE, rutaEstado);
Serial.println(FIREBASE.intData());
if (FIREBASE.intData() == 1)
{
  digitalWrite(LED, HIGH);
  digitalWrite(RELE, LOW);
}
else{
  digitalWrite(LED, LOW);
  digitalWrite(RELE, HIGH);
}
}