import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase, ref, onValue, set } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getAuth, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-auth.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBt1jm7KetdbdQ359oTyMMyxGVVHOPOynw",
  authDomain: "esp32-lm35-99837.firebaseapp.com",
  projectId: "esp32-lm35-99837",
  storageBucket: "esp32-lm35-99837.appspot.com",
  messagingSenderId: "1009221590592",
  appId: "1:1009221590592:web:00450d381c21ab681209a5"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

let TempRef = document.getElementById("temp")
let SistemaRef = document.getElementById("sistema")
let botonOnOffRef = document.getElementById("OnOffButton")
let navRef = document.getElementById("navId")
var correo;
var UID;

const RutaRef = ref(database, 'Sensor/');
onValue(RutaRef, (snapshot) => {
  const data = snapshot.val();
  TempRef.innerHTML = `
  <p style="font-size: 100px;">${data + "°C"}</p>
  `;
})

const OnOffRef = ref(database, '/OnOff/Estado');
onValue(OnOffRef, (snapshot) => {
  const dataOnOff = snapshot.val();
  if (dataOnOff == true){
    var dataOnOFF1 = "Estado: Encendido"
  }
  else{
    var dataOnOFF1 = "Estado: Apagado"
  }
  SistemaRef.innerHTML = `
  <p>${dataOnOFF1}</p>
  `;
})


onAuthStateChanged(auth, (user) => {
    if (user) {
      const uid = user.uid;
      UID = uid;
      console.log("Usuario actual:" + uid);
  
      const email = user.email;
      correo=email;
      console.log(email)
      botonOnOffRef.addEventListener("click", EnviarTrueFalse)
      navRef.innerHTML = `
      <li class="nav-item">
      <a class="navbar-brand" href="./index.html">
      <button class="btn btn-outline-warning me-3" type="button">Home</button>
      </a>
  </li>
  <li class="nav-item">
      <button class="btn btn-outline-warning me-3" type="button" id="logout">Cerrar sesion</button>
  </li>
      `
      let logoutRef = document.getElementById("logout")
      logoutRef.addEventListener("click", Logout)
    } else {
      console.log("Ningun usuario detectado ")
      botonOnOffRef.addEventListener("click", Ventana)
    }
  });
  
  function Ventana(){
    window.location.href = "pages/login.html"
  }

  function Logout(){
    signOut(auth).then(() => {
      // Sign-out successful.
      location.reload()
    }).catch((error) => {
      // An error happened.
    });
  }
  function EnviarTrueFalse(){
    const OnOffRef = ref(database, '/OnOff/Estado');
    onValue(OnOffRef, (snapshot) => {
      const dataOnOff = snapshot.val();
      if (dataOnOff == true){
        set(ref(database, "OnOff/"), {
            Estado: false,
        })
      }
      else{
        set(ref(database, "OnOff/"), {
            Estado: true,
        })
      }
    })
  }