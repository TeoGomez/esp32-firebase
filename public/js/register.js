import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase, ref, onValue, set } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getAuth, onAuthStateChanged, createUserWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-auth.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBt1jm7KetdbdQ359oTyMMyxGVVHOPOynw",
  authDomain: "esp32-lm35-99837.firebaseapp.com",
  projectId: "esp32-lm35-99837",
  storageBucket: "esp32-lm35-99837.appspot.com",
  messagingSenderId: "1009221590592",
  appId: "1:1009221590592:web:00450d381c21ab681209a5"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

let correoRefReg = document.getElementById("exampleInputEmail1")
let passRefReg = document.getElementById("exampleInputPassword1")
let buttonRefReg = document.getElementById("buttonRegister")
let NombreRefReg = document.getElementById("exampleInputName1");

buttonRefReg.addEventListener("click", RegistroUser);

function RegistroUser(){

    if((correoRefReg.value != '') && (passRefReg.value != '') && (NombreRefReg.value != '')){

    createUserWithEmailAndPassword(auth, correoRefReg.value, passRefReg.value, NombreRefReg.value)
  .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
            console.log("Usuario: " + user + " ID: " + user.uid);
            console.log("Creación de usuario.");

            let nombre = NombreRefReg.value
            let correo = correoRefReg.value

            set(ref(database, "usuarios/" + user.uid), {
                Nombre_Apellido: nombre,
                Correo: correo
            }).then(() => {
                window.location.href = "../index.html";
            })
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
    alert(errorMessage);
  });
}
else{
            alert("Revisar que los campos de usuario y contraseña esten completos.");
        }  
}
