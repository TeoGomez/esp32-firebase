import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase, ref, onValue } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getAuth, onAuthStateChanged, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-auth.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBt1jm7KetdbdQ359oTyMMyxGVVHOPOynw",
  authDomain: "esp32-lm35-99837.firebaseapp.com",
  projectId: "esp32-lm35-99837",
  storageBucket: "esp32-lm35-99837.appspot.com",
  messagingSenderId: "1009221590592",
  appId: "1:1009221590592:web:00450d381c21ab681209a5"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

let correoRefLog = document.getElementById("exampleInputEmail1")
let passRefLog = document.getElementById("exampleInputPassword1")
let buttonRefLog = document.getElementById("buttonLogin")

buttonRefLog.addEventListener("click", LogIn);

//funcion para Iniciar sesion

function LogIn (){

    if((correoRefLog.value != '') && (passRefLog.value != '')){

        signInWithEmailAndPassword(auth, correoRefLog.value, passRefLog.value)
        .then((userCredential) => {
            const user = userCredential.user;
            window.location.href = "../index.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}